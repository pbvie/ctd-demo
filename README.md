# ctd-demo

Welcome to Connecting the Dots - Demo System. 

## Overview

![System Overview](docs/img/system_overview.png)

## Notes

The purpose of this demo system is to show how Distributed Tracing can be implemented in Scala in 
different ways and for different tools. As such, this system does not deal with the following 
(important) concerns of real-world systems:

* thoughtful architecture and de-coupling of services
* error-handling
* validation
* correct handling of resources
* security

## Prerequisites

* sbt (tested with 1.2.7)
* Docker (tested with 1.18)
* docker-compose (tested with 1.23)
* Internet connection (if running for the first time)

## Running

### Infrastructure

The easiest way to bootstrap the required infrastructure (Kafka, Zookeeper and Jaeger) is to make use of the 
provided `docker-compose.yml` that can be found in `/infra/`.

Before we can start the containers we need to set the `KAFKA_ADVERTISED_HOST_NAME` in our `docker-compose.yml`. Open the file in your favorite editor and look for the following part:

```yaml
  kafka:
    image: wurstmeister/kafka
    container_name: ctd-kafka
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: your_ip_here
```

Replace `your_ip_here` with your actual ip address. Note: Using localhost or 127.0.0.1 WILL NOT work.

Now, open a terminal inside the project directory and run

```bash
docker-compose -f infra/docker-compose.yml up
```

If all goes well, you should see quite a lot of output for Kafka, Zookeeper and Jaeger. Among other lines you should see

```
ctd-jaeger   | {"level":"info","ts":1545308541.0235963,"caller":"healthcheck/handler.go:133","msg":"Health Check state change","status":"ready"}
...
...
...
ctd-kafka    | Created topic "ctd-order-events".
```

### demo-system

First we have to set some environment variables to tell the demo system about Kafka and Jaeger. pen a new terminal inside the project directory and run

```bash
export JAEGER_HOST=`docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ctd-jaeger`
```
```bash
export KAFKA_HOST=`docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ctd-kafka`
```

After that (in the same terminal), run

```bash
sbt
```

and inside the sbt-shell
```bash
runAll
```

You should see

```
user.UserService$ - Bootstrapping user service...
order.OrderService$ - Bootstrapping order service...
preparation.PreparationService$ - Bootstrapping preparation service...
preparation.KafkaOrderConsumer - Starting order event consumption on topic ctd-order-events
```

The system is now ready to accept orders!

## Creating an order

New orders are created by sending an order request to `localhost:8080`:

```http
PUT /order HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Correlation-Id: 896fea65-bb67-4f31-89c4-759c5149fd94
cache-control: no-cache
{
	"meta": {
		"transactionId": "3b7f104c-b5d8-48de-9e38-87847dfdcdd8",
		"transactionTimestamp": "2018-11-12T10:15:30.00Z"
	},
	"order": {
		"id": "b40efb56-3db4-48c2-9ce0-ca050110a8ea",
    	"username": "cathy",
    	"beverage": "hot wine"
	}
}
```

With cURL this would look like the following

```bash
curl -X PUT \
  http://localhost:8080/order \
  -H 'Content-Type: application/json' \
  -H 'Correlation-Id: 36860bf3-a237-4dd0-a81d-779ea2c20e55' \
  -H 'cache-control: no-cache' \
  -d '{
	"meta": {
		"transactionId": "56e96796-975e-4caf-93d1-b6361ad81055",
		"transactionTimestamp": "2018-11-12T10:15:30.00Z"
	},
	"order": {
		"id": "e7300d71-386d-4c2c-ab1e-c2a01dfc6ca4",
    	"username": "cathy",
    	"beverage": "hot wine"
	}
}'
```

If you open `http://localhost:16686/search` in your browser and select `ctd-order-service` you should now see a trace like the following:

![Trace](docs/img/trace.png)

## Note: Removing containers from previous runs

For a fresh start you might want to remove containers from previous runs:

```bash
docker rm ctd-zookeeper
docker rm ctd-kafka
docker rm ctd-jaeger
```

## License

This code is open source software licensed under the
[Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0) license.

## Credits

![Logo](docs/img/logo_small.png)

Icon made by [Freepik](http://www.freepik.com/) from [flaticon.com](http://www.flaticon.com/)
