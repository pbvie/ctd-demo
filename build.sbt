// *****************************************************************************
// Projects
// *****************************************************************************

lazy val `ctd-demo` =
  project
    .in(file("."))
    .aggregate(
      `ctd-tracing-lib`,
      `ctd-protocol`,
      `ctd-user-service`,
      `ctd-order-service`,
      `ctd-preparation-service`
    )

lazy val `ctd-protocol` =
  project
    .in(file("protocol"))
    .enablePlugins(AutomateHeaderPlugin)
    .settings(settings)
    .settings(scalaPbSettings)
    .settings(
      libraryDependencies ++= Seq(
        library.grpcNetty,
        library.scalaPbRuntimeGrpc,
        library.scalaPbRuntime      % "protobuf",
        library.scalaCheck          % Test,
        library.utest               % Test
      ) ++ library.log4j
    )

lazy val `ctd-tracing-lib` =
  project
    .in(file("tracing-lib"))
    .enablePlugins(AutomateHeaderPlugin)
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.akkaActor    % Provided,
        library.akkaHttp     % Provided,
        library.jaegerClient % Provided,
        library.kafkaClient  % Provided,
        library.log4jApi     % Provided,
        library.log4jScala   % Provided,
        library.scalaCheck   % Test,
        library.utest        % Test
      )
    )

lazy val `ctd-user-service` =
  project
    .in(file("services/user"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(`ctd-protocol`)
    .dependsOn(`ctd-tracing-lib`)
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.circeGeneric,
        library.jaegerClient,
        library.opentracingGrpc,
        library.pureConfig,
        library.scalaCheck % Test,
        library.utest      % Test
      ) ++ library.log4j
    )

lazy val `ctd-order-service` =
  project
    .in(file("services/order"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(`ctd-protocol`)
    .dependsOn(`ctd-tracing-lib`)
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.akkaHttp,
        library.akkaHttpJson,
        library.akkaLog4j,
        library.circeGeneric,
        library.jaegerClient,
        library.kafkaClient,
        library.opentracingGrpc,
        library.pureConfig,
        library.scalaCheck % Test,
        library.utest      % Test
      ) ++ library.log4j
    )

lazy val `ctd-preparation-service` =
  project
    .in(file("services/preparation"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(`ctd-protocol`)
    .dependsOn(`ctd-tracing-lib`)
    .settings(settings)
    .settings(
      libraryDependencies ++= Seq(
        library.akkaActor,
        library.akkaLog4j,
        library.jaegerClient,
        library.kafkaClient,
        library.pureConfig,
        library.scalaCheck % Test,
        library.utest      % Test
      ) ++ library.log4j
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      val akka            = "2.5.19"
      val akkaHttp        = "10.1.6"
      val akkaHttpJson    = "1.23.0"
      val akkaLog4j       = "1.6.1"
      val circe           = "0.11.0"
      val grpcNetty       = scalapb.compiler.Version.grpcJavaVersion
      val jaeger          = "0.32.0"
      val kafkaClient     = "2.1.0"
      val log4j           = "2.11.1"
      val log4jScala      = "11.0"
      val jackson         = "2.9.8"
      val opentracing     = "0.31.0"
      val opentracingGrpc = "0.0.10"
      val pureConfig      = "0.10.1"
      val scalaCheck      = "1.14.0"
      val scalaPb         = scalapb.compiler.Version.scalapbVersion
      val utest           = "0.6.6"
    }
    val akkaActor          = "com.typesafe.akka"               %% "akka-actor"              % Version.akka
    val akkaHttp           = "com.typesafe.akka"               %% "akka-http"               % Version.akkaHttp
    val akkaHttpJson       = "de.heikoseeberger"               %% "akka-http-circe"         % Version.akkaHttpJson
    val akkaLog4j          = "de.heikoseeberger"               %% "akka-log4j"              % Version.akkaLog4j
    val kafkaClient        = "net.cakesolutions"               %% "scala-kafka-client"      % Version.kafkaClient
    val circeGeneric       = "io.circe"                        %% "circe-generic"           % Version.circe
    val grpcNetty          = "io.grpc"                          % "grpc-netty"              % Version.grpcNetty
    val jaegerClient       = "io.jaegertracing"                 % "jaeger-client"           % Version.jaeger
    val log4jApi           = "org.apache.logging.log4j"         % "log4j-api"               % Version.log4j
    val log4jCore          = "org.apache.logging.log4j"         % "log4j-core"              % Version.log4j
    val log4jScala         = "org.apache.logging.log4j"        %% "log4j-api-scala"         % Version.log4jScala
    val log4jslf4j         = "org.apache.logging.log4j"         % "log4j-slf4j-impl"        % Version.log4j
    val jacksonDb          = "com.fasterxml.jackson.core"       % "jackson-databind"        % Version.jackson
    val jacksonYaml        = "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % Version.jackson
    val opentracingGrpc    = "io.opentracing.contrib"           % "opentracing-grpc"        % Version.opentracingGrpc
    val pureConfig         = "com.github.pureconfig"           %% "pureconfig"              % Version.pureConfig
    val scalaCheck         = "org.scalacheck"                  %% "scalacheck"              % Version.scalaCheck
    val scalaPbRuntime     = "com.thesamet.scalapb"            %% "scalapb-runtime"         % Version.scalaPb
    val scalaPbRuntimeGrpc = "com.thesamet.scalapb"            %% "scalapb-runtime-grpc"    % Version.scalaPb
    val utest              = "com.lihaoyi"                     %% "utest"                   % Version.utest

    val log4j = Seq(log4jApi, log4jCore, log4jScala, log4jslf4j, jacksonDb, jacksonYaml)
  }

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val settings =
  commonSettings ++
  scalafmtSettings

lazy val commonSettings =
  Seq(
    scalaVersion := "2.12.8",
    organization := "io.ontherocks",
    organizationName := "Petra Bierleutgeb",
    startYear := Some(2018),
    licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0")),
    resolvers ++= Seq(
      Resolver.sonatypeRepo("snapshots"),
      Resolver.bintrayRepo("cakesolutions", "maven")
    ),
    scalacOptions ++= Seq(
      "-unchecked",
      "-deprecation",
      "-language:higherKinds",
      "-target:jvm-1.8",
      "-encoding", "UTF-8",
      "-Ypartial-unification",
      "-Ywarn-dead-code",
      "-Ywarn-extra-implicit",
      "-Ywarn-inaccessible",
      "-Ywarn-infer-any",
      "-Ywarn-nullary-override",
      "-Ywarn-nullary-unit",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates",
      "-Ywarn-value-discard"
    ),
    Compile / unmanagedSourceDirectories := Seq((Compile / scalaSource).value),
    Test / unmanagedSourceDirectories := Seq((Test / scalaSource).value),
    testFrameworks += new TestFramework("utest.runner.Framework"),
    outputStrategy := Some(StdoutOutput)
)

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true
  )

lazy val scalaPbSettings =
  PB.targets in Compile := Seq(
    scalapb.gen(flatPackage=true) -> (sourceManaged in Compile).value
  )

addCommandAlias("validate", ";clean;scalafmtCheck;test:scalafmtCheck;test")
addCommandAlias("runAll", ";ctd-user-service/reStart;ctd-order-service/reStart;ctd-preparation-service/reStart")
addCommandAlias("stop", ";ctd-user-service/reStop;ctd-order-service/reStop;ctd-preparation-service/reStop")
