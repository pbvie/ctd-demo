/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import cakesolutions.kafka.KafkaProducer
import io.ontherocks.ctd.protocol.event.order.OrderEvent
import io.ontherocks.ctd.tracing.KafkaHeaderInjector
import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext
import io.opentracing.propagation.Format
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future }

trait EventPublisher {

  def publishOrder(order: domain.Order)(implicit ec: ExecutionContext,
                                        tc: TracingContext): Future[String]

}

class KafkaEventPublisher(serviceConfig: ServiceConfig,
                          producer: KafkaProducer[Array[Byte], Array[Byte]])
    extends EventPublisher
    with Logging {

  import io.ontherocks.ctd.tracing.extra.ExTra._

  override def publishOrder(
      order: domain.Order
  )(implicit ec: ExecutionContext, tc: TracingContext): Future[String] =
    withChildSpanAsync("publish-order") { tc =>
      logger.info(s"Publishing order: $order")
      tc.span.setTag("topic", serviceConfig.orderPublishTopic)
      val orderEvent = OrderEvent(order.id.toString, order.username, order.beverage)
      val record =
        new ProducerRecord(serviceConfig.orderPublishTopic,
                           order.username.toString.getBytes,
                           orderEvent.toByteArray)
      tc.tracer.inject(tc.span.context(),
                       Format.Builtin.TEXT_MAP,
                       new KafkaHeaderInjector(record.headers()))
      producer.send(record).map(_.offset().toString)
    }

}
