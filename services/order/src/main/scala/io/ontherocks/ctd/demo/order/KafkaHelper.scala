/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import cakesolutions.kafka.KafkaProducer
import org.apache.kafka.common.serialization.ByteArraySerializer
import org.apache.logging.log4j.scala.Logging

trait KafkaHelper extends Logging {

  def createProducer(config: KafkaConfig): KafkaProducer[Array[Byte], Array[Byte]] = {
    val producerConf = KafkaProducer.Conf(
      bootstrapServers = s"${config.host}:${config.port}",
      keySerializer = new ByteArraySerializer,
      valueSerializer = new ByteArraySerializer
    )
    KafkaProducer(producerConf)
  }

}

object KafkaHelper extends KafkaHelper
