/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future }

trait OrderHandler {

  def handleOrder(
      order: domain.Order
  )(implicit ec: ExecutionContext, tc: TracingContext): Future[String]

}

class DefaultOrderHandler(userClient: UserClient, eventPublisher: EventPublisher)
    extends OrderHandler
    with Logging {

  import io.ontherocks.ctd.tracing.extra.ExTra._

  override def handleOrder(
      order: domain.Order
  )(implicit ec: ExecutionContext, tc: TracingContext): Future[String] = {
    logger.info(s"Handling order: $order")
    // we don't create a new span, TracinContext is passed on implicitly
    userClient.getUser(order.username).flatMap {
      case Some(_) =>
        logger.debug(s"User ${order.username} exists. Publishing order.")
        // again, TracingContext is passed on implicitly
        eventPublisher.publishOrder(order).map { _ =>
          logger.trace(s"Successfully published order")
          "order created"
        }
      case None =>
        logger.warn(s"Username ${order.username} not found. Cancelling order.")
        Future.successful("error: user not found")
    }
  }
}
