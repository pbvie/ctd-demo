/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.{ ActorMaterializer, Materializer }
import io.ontherocks.ctd.tracing.TracingBootstrap.jaegerTracer
import io.opentracing.Tracer
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future, Promise }

class OrderService(serviceConfig: ServiceConfig)(implicit tracer: Tracer) {

  import KafkaHelper._

  implicit private val system: ActorSystem  = ActorSystem("order")
  implicit private val mat: Materializer    = ActorMaterializer()
  implicit private val ec: ExecutionContext = system.dispatcher

  private val isShutdown: Promise[Unit] = Promise()

  def start(): Future[Unit] = {
    val producer       = createProducer(serviceConfig.kafka)
    val eventPublisher = new KafkaEventPublisher(serviceConfig, producer)
    val userClient =
      new GrpcUserClient(serviceConfig.userService.host, serviceConfig.userService.port)
    val handler          = new DefaultOrderHandler(userClient, eventPublisher)
    val routeDefinitions = new DefaultRouteDefinitions(handler, tracer)
    Http()
      .bindAndHandle(routeDefinitions.routes(), "localhost", 8080)
      .flatMap(_ => isShutdown.future)
  }

  def stop(): Unit =
    // cleanup resources
    isShutdown.success(())

}

object OrderService extends Logging {

  def bootstrap(serviceConfig: ServiceConfig): OrderService = {
    logger.debug("Bootstrapping order service...")
    val tracer = jaegerTracer(serviceConfig.serviceName, serviceConfig.jaeger)
    new OrderService(serviceConfig)(tracer)
  }

}
