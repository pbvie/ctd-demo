/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ Directive, Route }
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.ontherocks.ctd.tracing.AkkaHttpHeaderExtractor
import io.ontherocks.ctd.tracing.extra.AkkaHttpTracingSupport
import io.opentracing.propagation.Format
import io.opentracing.{ SpanContext, Tracer }
import org.apache.logging.log4j.scala.{ Logger, Logging }

import scala.concurrent.ExecutionContext

trait RouteDefinitions {

  protected def logger: Logger
  protected def handler: OrderHandler
  protected def tracer: Tracer

  import AkkaHttpTracingSupport._
  import io.circe.generic.auto._
  import io.ontherocks.ctd.tracing.extra.ExTra._

  private def extractSpanContext =
    Directive[Tuple1[SpanContext]] { inner => ctx =>
      {
        val headers = ctx.request.headers
        val context = tracer.extract(Format.Builtin.TEXT_MAP, new AkkaHttpHeaderExtractor(headers))
        inner(Tuple1(context))(ctx)
      }
    }

  private def serviceRoutes()(implicit system: ActorSystem, ec: ExecutionContext) =
    path("order") {
      put {
        extractRequest { request =>
          entity(as[http.CreateOrderRequest]) { createOrderRequest =>
            logger.debug(s"Received new create order request: $createOrderRequest")
            val span = buildRootSpan("handle-order-request")(tracer) // create root span
            span.setBaggageItem("transaction-id", createOrderRequest.meta.transactionId.toString) // set a baggage item, it will be transported across process boundaries
            implicit val tc
              : TracingContext = TracingContext(tracer, span) // create initial tracing context
            val result = withSpanAsync { implicit tc => // withSpanAsync makes sure the span is closed, even in case of errors
              // just for demo purposes: resend request to ourselves
              Http().singleRequest(
                HttpRequest(
                  method = HttpMethods.PUT,
                  uri =
                    s"http://${request.uri.authority.host}:${request.uri.authority.port}/order-with-context",
                  headers = tracingHeaders(),
                  entity = request.entity
                )
              )
            }
            complete(result)
          }
        }
      }
    } ~
    path("order-with-context") {
      put {
        extractSpanContext { spanContext => // automatically extracted span context
          entity(as[http.CreateOrderRequest]) { createOrderRequest =>
            implicit val tc: TracingContext =
              TracingContext.childOf("handle-order-with-context", tracer, spanContext) // create a new child span
            val order = domain.Order(createOrderRequest.order.id,
                                     createOrderRequest.order.username,
                                     createOrderRequest.order.beverage)
            val result = withSpanAsync { implicit tc => // handle order and finish child span on completion
              handler
                .handleOrder(order)
                .map(http.CreateOrderResponse)
            }
            complete(result)
          }
        }
      }
    }

  def routes()(implicit system: ActorSystem, ec: ExecutionContext): Route = serviceRoutes()

}

final class DefaultRouteDefinitions(protected val handler: OrderHandler,
                                    protected val tracer: Tracer)
    extends RouteDefinitions
    with Logging
