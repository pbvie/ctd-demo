/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.order

import java.util.UUID

import io.grpc.ManagedChannelBuilder
import io.ontherocks.ctd.protocol.grpc
import io.ontherocks.ctd.protocol.grpc.user.{ GetUserRequest, UserServiceGrpc }
import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext
import io.opentracing.Tracer
import io.opentracing.contrib.grpc.ClientTracingInterceptor
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future }

trait UserClient {

  def getUser(username: String)(implicit ec: ExecutionContext,
                                tc: TracingContext): Future[Option[domain.User]]

}

class GrpcUserClient(host: String, port: Int)(implicit tracer: Tracer)
    extends UserClient
    with Logging {

  import io.ontherocks.ctd.tracing.extra.ExTra._

  private val tracingClientInterceptor = new ClientTracingInterceptor(tracer)

  private val channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build
  private val client  = UserServiceGrpc.stub(tracingClientInterceptor.intercept(channel))

  private def transform(user: grpc.user.User): domain.User =
    domain.User(id = UUID.fromString(user.userId),
                username = user.username,
                email = user.email,
                memberSinceDays = user.memberSinceDays)

  override def getUser(
      username: String
  )(implicit ec: ExecutionContext, tc: TracingContext): Future[Option[domain.User]] =
    withChildSpanAsync("get-user") { tc =>
      /*
       * Unfortunately, most opentracing instrumentations rely on the accessing the currently
       * active span via OpenTracing's ScopeManager. For this reason, when using third-party
       * tracing instrumentations it's necessary to explicitly set the active span.
       * Note: In this example, we're using opentracing-contrib/java-grpc for the tracing of
       * gRPC communication.
       */
      tracer.scopeManager().activate(tc.span, false)
      val request = GetUserRequest(username)
      client
        .get(request)
        .map { response =>
          logger.trace(s"Received user response: $response")
          response.user.map(transform)
        }
    }

}
