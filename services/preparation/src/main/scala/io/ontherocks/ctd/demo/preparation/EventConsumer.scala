/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation

import io.ontherocks.ctd.protocol.event.order.OrderEvent
import io.opentracing.Tracer
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec
import scala.concurrent.{ ExecutionContext, Future, blocking }

trait EventConsumer {

  def startConsumption()(implicit ec: ExecutionContext): Future[Unit]

}

class KafkaOrderConsumer(serviceConfig: ServiceConfig,
                         consumer: KafkaConsumer[Array[Byte], Array[Byte]],
                         eventHandler: EventHandler[OrderEvent],
                         tracer: Tracer)
    extends EventConsumer
    with Logging {

  import io.ontherocks.ctd.tracing.extra.ExTra._
  import io.ontherocks.ctd.tracing.extra.KafkaTracingSupport._

  import collection.JavaConverters._

  override def startConsumption()(implicit ec: ExecutionContext): Future[Unit] =
    Future {
      @tailrec
      def rec(): Unit = {
        logger.trace("Polling order events...")
        val records = blocking(consumer.poll(java.time.Duration.ofMillis(3000)))
        logger.trace(s"Consumed ${records.count()} order events.")
        records.iterator().asScala.foreach { record =>
          val extractedSpanContext = extractSpanContext(tracer, record) // extract span context from Kafka headers
          val followingContext =
            TracingContext(tracer,
                           buildFollowsFrom("consume-order-event", tracer, extractedSpanContext))
          withSpan { implicit tc =>
            val event = OrderEvent.parseFrom(record.value)
            eventHandler.handle(event)
          }(followingContext)
        }
        blocking(consumer.commitSync())
        rec()
      }
      consumer.subscribe(List(serviceConfig.orderConsumerTopic).asJava)
      logger.info(s"Starting order event consumption on topic ${serviceConfig.orderConsumerTopic}")
      rec()
    }

}
