/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation

import java.util.UUID

import akka.actor.ActorRef
import io.ontherocks.ctd.protocol.event.order.OrderEvent
import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.ExecutionContext

trait EventHandler[T] {

  def handle(event: T)(implicit ec: ExecutionContext, tc: TracingContext): Unit

}

class OrderEventHandler(overseer: ActorRef) extends EventHandler[OrderEvent] with Logging {

  import io.ontherocks.ctd.tracing.extra.ExTra._
  import io.ontherocks.ctd.demo.preparation.sim.Protocol._

  override def handle(event: OrderEvent)(implicit ec: ExecutionContext, tc: TracingContext): Unit =
    withChildSpan("handle-order-event") { tc =>
      logger.debug(s"Consumed order event: $event")
      val order = domain.Order(UUID.fromString(event.id), event.username, event.beverage)
      overseer ! TracedOverseerMessage(NewOrder(order), tc)
    }
}
