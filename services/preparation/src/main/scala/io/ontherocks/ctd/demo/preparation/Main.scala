/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext }

object Main extends App with Logging {

  implicit val ec: ExecutionContext = ExecutionContext.global

  logger.debug("Starting Preparation Service...")

  val serviceConfig = ServiceConfig.load()
  logger.debug(s"Loaded service config: $serviceConfig")

  val preparationService = PreparationService.bootstrap(serviceConfig)

  val preparationServiceShutdownAlert = preparationService.start()

  Await.ready(preparationServiceShutdownAlert, Duration.Inf)

  logger.debug(s"Shutting down Preparation Service - bye!")

}
