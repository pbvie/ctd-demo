/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation

import akka.actor.ActorSystem
import io.ontherocks.ctd.demo.preparation.sim.Overseer
import io.ontherocks.ctd.tracing.TracingBootstrap.jaegerTracer
import io.opentracing.Tracer
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future, Promise }

class PreparationService(serviceConfig: ServiceConfig, tracer: Tracer) {

  import KafkaHelper._

  implicit private val actorSystem: ActorSystem = ActorSystem("preparation")
  implicit private val ec: ExecutionContext     = actorSystem.dispatcher

  private val isShutdown: Promise[Unit] = Promise()

  def start(): Future[Unit] = {
    val consumer      = createConsumer(serviceConfig.kafka)
    val overseer      = actorSystem.actorOf(Overseer.props, "mike")
    val eventHandler  = new OrderEventHandler(overseer)
    val eventConsumer = new KafkaOrderConsumer(serviceConfig, consumer, eventHandler, tracer)
    eventConsumer
      .startConsumption()
      .flatMap(_ => isShutdown.future)
  }

  def stop(): Unit =
    // cleanup resources
    isShutdown.success(())

}

object PreparationService extends Logging {

  def bootstrap(serviceConfig: ServiceConfig): PreparationService = {
    logger.debug("Bootstrapping preparation service...")
    val tracer = jaegerTracer(serviceConfig.serviceName, serviceConfig.jaeger)
    new PreparationService(serviceConfig, tracer)
  }

}
