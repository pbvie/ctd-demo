/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation

import io.ontherocks.ctd.tracing.TracingConfig
import pureconfig.loadConfigOrThrow
import pureconfig.generic.auto._

object ServiceConfig {

  private val ConfigPath = "io.ontherocks.ctd.demo.preparation"

  def load(): ServiceConfig = loadConfigOrThrow[ServiceConfig](ConfigPath)

}

final case class ServiceConfig(serviceName: String,
                               orderConsumerTopic: String,
                               kafka: KafkaConfig,
                               jaeger: TracingConfig)

final case class KafkaConfig(host: String, port: Int)
