/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.preparation

import akka.actor.{ Actor, ActorRef, Props }
import akka.event.Logging
import io.ontherocks.ctd.demo.preparation.domain.Order
import io.ontherocks.ctd.tracing.extra.ActorTracingSupport
import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext

import scala.concurrent.{ ExecutionContext, Future, blocking }
import scala.util.Random

/**
  * Simulates work done by humans :)
  */
object sim {

  object Protocol {

    /*
     * Akka Actors provide no functionality to attach additional meta data to a message, so the
     * tracing information has to be transported in the message itself.
     */

    sealed trait OverseerMessage
    final case class NewOrder(order: Order)       extends OverseerMessage
    final case class OrderCompleted(order: Order) extends OverseerMessage
    final case class TracedOverseerMessage(message: OverseerMessage, tc: TracingContext)

    sealed trait WorkerMessage
    final case class ProcessOrder(order: Order) extends WorkerMessage
    final case class TracedWorkerMessage(message: WorkerMessage, tc: TracingContext)

  }

  object Overseer {
    def props = Props(new Overseer)
  }

  /**
    * Overseer receives new incoming orders and distributes them to workers.
    */
  class Overseer extends Actor with ActorTracingSupport {

    private val logger = Logging(context.system, this)

    private val workers = List(context.system.actorOf(Worker.props(30), "andy"),
                               context.system.actorOf(Worker.props(50), "bob"),
                               context.system.actorOf(Worker.props(20), "chris"))

    // random worker selected by fair dice roll
    private def selectRandomWorker(workers: List[ActorRef]): ActorRef = {
      val selected = Random.nextInt(workers.size)
      workers(selected)
    }

    import Protocol._
    override def receive: Receive = {
      case TracedOverseerMessage(NewOrder(order), parentTc) =>
        withActorTracing("distribute-order", parentTc) { tc =>
          logger.info(s"Distributing $order to worker")
          val selectedWorker = selectRandomWorker(workers)
          selectedWorker ! TracedWorkerMessage(ProcessOrder(order), tc)
        }
      case TracedOverseerMessage(OrderCompleted(order), parentTc) =>
        withActorTracing("notify-customer", parentTc) { _ =>
          logger.info(
            s"*** Good news ${order.username} - your ${order.beverage} is ready! \\o/ ***"
          )
        }
      case msg => logger.warning(s"Received unknown message $msg")
    }

  }

  object Worker {
    def props(preparationSeconds: Int) = Props(new Worker(preparationSeconds))
  }

  /**
    * Workers receive orders from the Overseer and prepare the requested beverage.
    */
  class Worker(preparationSeconds: Int) extends Actor with ActorTracingSupport {
    import Protocol._

    private implicit val ec: ExecutionContext = context.dispatcher

    private val logger = Logging(context.system, this)

    // note: in real life blocking op should use a dedicated dispatcher
    def prepareOrder(order: Order)(implicit ec: ExecutionContext): Future[Order] = Future {
      blocking(Thread.sleep(preparationSeconds.toLong)) // speed things up
      order
    }

    override def receive: Receive = {
      case TracedWorkerMessage(ProcessOrder(order), parentTc) => {
        withActorTracingAsync("prepare-beverage", parentTc) { tc =>
          logger.info(s"Working on order $order")
          val overseer = sender()
          prepareOrder(order).map { _ =>
            logger.info(s"Completed order $order")
            overseer ! TracedOverseerMessage(OrderCompleted(order), tc)
          }
        }
        ()
      }
      case msg => logger.warning(s"Received unknown message $msg")
    }
  }

}
