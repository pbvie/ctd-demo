/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.user

import io.grpc.{ ServerBuilder, ServerServiceDefinition }
import io.opentracing.Tracer
import io.opentracing.contrib.grpc.ServerTracingInterceptor

trait GrpcServer {

  def runServer(ssd: ServerServiceDefinition, port: Int)(implicit tracer: Tracer): Unit = {

    val tracingInterceptor = new ServerTracingInterceptor(tracer)

    val server = ServerBuilder
      .forPort(port)
      .addService(tracingInterceptor.intercept(ssd))
      .build
      .start

    // make sure server is stopped when jvm is shut down
    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        server.shutdown()
        ()
      }
    })

  }

}

object GrpcServer extends GrpcServer
