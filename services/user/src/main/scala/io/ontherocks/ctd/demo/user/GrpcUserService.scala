/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.user

import java.util.UUID

import io.ontherocks.ctd.protocol.grpc.user.{ User => ProtoUser, _ }
import io.opentracing.Tracer
import io.opentracing.log.Fields
import io.opentracing.tag.Tags
import org.apache.logging.log4j.scala.Logging

import scala.collection.JavaConverters._
import scala.concurrent.{ ExecutionContext, Future }

object GrpcUserService {
  final case class User(id: UUID, username: String, email: String, memberSinceDays: Int)
}

class GrpcUserService(tracer: Tracer) extends UserServiceGrpc.UserService with Logging {

  import GrpcUserService._
  import io.ontherocks.ctd.tracing.extra.ExTra._

  private implicit val ec: ExecutionContext = ExecutionContext.global

  private val db = {
    val userA = User(UUID.randomUUID(), "anna", "anna@example.org", 10)
    val userB = User(UUID.randomUUID(), "barbara", "barbara@example.org", 2)
    val userC = User(UUID.randomUUID(), "cathy", "cathy@example.org", 20)
    Map(
      userA.username -> userA,
      userB.username -> userB,
      userC.username -> userC
    )
  }

  private def findUserByUsername(
      username: String
  )(implicit ec: ExecutionContext, tc: TracingContext): Future[Option[User]] =
    withChildSpanAsync("find-user-by-username") { tc =>
      Future {
        tc.span.setTag(Tags.DB_TYPE.getKey, "hashmap")
        val user = db.get(username)
        if (user.isEmpty) {
          tc.span.setTag(Tags.ERROR.getKey, true)
          tc.span.log(
            Map(
              Fields.EVENT        -> "error",
              Fields.ERROR_OBJECT -> new IllegalArgumentException("user not found") // just for demo purposes
            ).asJava
          )
        }
        user
      }
    }

  override def get(
      request: GetUserRequest
  ): Future[GetUserResponse] = {
    val span                        = tracer.scopeManager().active().span()
    implicit val tc: TracingContext = TracingContext(tracer, span)
    logger.info(s"Received request to get user ${request.username}")
    val maybeUser = findUserByUsername(request.username).map {
      case Some(existingUser) =>
        Some(
          ProtoUser(userId = existingUser.id.toString,
                    username = existingUser.username,
                    email = existingUser.email,
                    memberSinceDays = existingUser.memberSinceDays)
        )
      case None =>
        None
    }
    maybeUser.map(GetUserResponse(_))
  }

}
