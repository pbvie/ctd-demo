/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.demo.user

import io.ontherocks.ctd.protocol.grpc.user.UserServiceGrpc
import io.ontherocks.ctd.tracing.TracingBootstrap
import io.opentracing.Tracer
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.{ ExecutionContext, Future, Promise }

class UserService(serviceConfig: ServiceConfig)(
    implicit val tracer: Tracer
) extends Logging {

  private implicit val ec: ExecutionContext = ExecutionContext.global
  private val isShutdown: Promise[Unit]     = Promise()

  def start(): Future[Unit] = {
    import GrpcServer._
    val ssd = UserServiceGrpc.bindService(new GrpcUserService(tracer), ec)
    runServer(ssd, serviceConfig.grpc.port)
    isShutdown.future
  }

  def stop(): Unit =
    isShutdown.success(())

}

object UserService extends Logging {

  import TracingBootstrap._

  def bootstrap(serviceConfig: ServiceConfig): UserService = {
    logger.debug("Bootstrapping user service...")
    val tracer = jaegerTracer(serviceConfig.serviceName, serviceConfig.jaeger)
    new UserService(serviceConfig)(tracer)
  }

}
