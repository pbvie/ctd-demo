/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing
import java.util
import java.util.{ Map => JavaMap }

import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.model.headers.CustomHeader
import io.opentracing.propagation.TextMap

import scala.collection.mutable.ListBuffer

class AkkaHttpHeaderInjector(private val headers: ListBuffer[HttpHeader]) extends TextMap {

  override def iterator(): util.Iterator[JavaMap.Entry[String, String]] =
    throw new UnsupportedOperationException(
      "should only be used with Tracer.inject()"
    )

  override def put(k: String, v: String): Unit = {
    val header = new CustomHeader {
      override def name(): String               = k
      override def value(): String              = v
      override def renderInRequests(): Boolean  = true
      override def renderInResponses(): Boolean = true
    }
    headers += header
  }

}
