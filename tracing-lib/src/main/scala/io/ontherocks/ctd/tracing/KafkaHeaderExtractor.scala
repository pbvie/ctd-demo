/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing

import java.util
import java.util.{ Map => JavaMap }

import io.opentracing.propagation.TextMap
import org.apache.kafka.common.header.Headers

import collection.JavaConverters._

class KafkaHeaderExtractor(private val headers: Headers) extends TextMap {

  override def iterator(): util.Iterator[JavaMap.Entry[String, String]] = {
    val m = headers.iterator().asScala.map(h => (h.key(), new String(h.value()))).toMap
    m.asJava.entrySet().iterator()
  }

  override def put(key: String, value: String): Unit = throw new UnsupportedOperationException(
    "should only be used with Tracer.extract()"
  )

}
