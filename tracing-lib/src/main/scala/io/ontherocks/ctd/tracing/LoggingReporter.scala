/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing

import java.util

import io.jaegertracing.internal.{ JaegerSpan, LogData }
import io.jaegertracing.spi.Reporter
import org.apache.logging.log4j.scala.{ Logger, Logging }

import scala.collection.JavaConverters._

trait LoggingReporter extends Reporter {

  protected def logger: Logger

  override def report(span: JaegerSpan): Unit =
    logger.trace(
      s"Reporting span: [$span] " +
      s"with tags ${span.getTags} " +
      s"with baggage items ${span.context().baggageItems()}" +
      s"and logs ${Option(span.getLogs).getOrElse(new util.ArrayList[LogData]()).asScala.map(_.getMessage)}"
    )

  override def close(): Unit = logger.trace("Closing tracer. Bye bye!")
}

object LoggingReporter extends LoggingReporter with Logging
