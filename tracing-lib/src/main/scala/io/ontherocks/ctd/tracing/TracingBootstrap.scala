/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing

import java.util.concurrent.atomic.AtomicReference

import io.jaegertracing.internal.JaegerTracer
import io.jaegertracing.internal.reporters.{ CompositeReporter, RemoteReporter }
import io.jaegertracing.internal.samplers.ConstSampler
import io.jaegertracing.thrift.internal.senders.UdpSender
import io.opentracing.util.GlobalTracer
import org.apache.logging.log4j.scala.Logging

trait TracingBootstrap extends Logging {

  private val remoteReporterRef    = new AtomicReference[RemoteReporter]()
  private val compositeReporterRef = new AtomicReference[CompositeReporter]()

  def jaegerTracer(tracerName: String, tracingConfig: TracingConfig): JaegerTracer = {
    remoteReporterRef.set(
      new RemoteReporter.Builder()
        .withSender(new UdpSender(tracingConfig.host, tracingConfig.port, 0))
        .build()
    )
    compositeReporterRef.set(new CompositeReporter(LoggingReporter, remoteReporterRef.get()))
    val jt = new JaegerTracer.Builder(tracerName)
      .withReporter(compositeReporterRef.get())
      .withSampler(new ConstSampler(true))
      .build()
    GlobalTracer.register(jt)
    jt
  }

}

object TracingBootstrap extends TracingBootstrap
