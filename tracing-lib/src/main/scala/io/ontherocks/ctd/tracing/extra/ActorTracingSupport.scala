/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing.extra
import akka.actor.Actor
import io.ontherocks.ctd.tracing.extra.ExTra.{ Tag, TracingContext }
import io.opentracing.tag.Tags

import scala.concurrent.{ ExecutionContext, Future }

trait ActorTracingSupport extends ExTra { this: Actor =>

  private final val COMPONENT_NAME = "Akka"

  protected def spanPrefix: Option[String] = None

  private def fullOpName(operationName: String): String =
    spanPrefix.map(p => s"$p.operationName").getOrElse(operationName)

  private def actorTags(): List[Tag] = List(
    Tag(Tags.COMPONENT.getKey, COMPONENT_NAME),
    Tag("actor.path", self.path.toStringWithoutAddress),
    Tag("actor.name", self.path.name)
  )

  def withActorTracing[T](operationName: String, tc: TracingContext)(op: TracingContext => T): T = {
    val prefixedOpName = fullOpName(operationName)
    val tags           = actorTags()
    super.withFollowsFrom(prefixedOpName, tags)(op)(tc)
  }

  def withActorTracingAsync[T](operationName: String, tc: TracingContext)(
      op: TracingContext => Future[T]
  )(implicit ec: ExecutionContext): Future[T] = {
    val prefixedOpName = fullOpName(operationName)
    val tags           = actorTags()
    super.withFollowsFromAsync(prefixedOpName, tags)(op)(ec, tc)
  }

}
