/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing
package extra

import akka.http.scaladsl.model.HttpHeader
import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext
import io.opentracing.propagation.Format

import scala.collection.mutable.ListBuffer

trait AkkaHttpTracingSupport {

  def tracingHeaders()(implicit tc: TracingContext): Seq[HttpHeader] = {
    val traceHeaders = ListBuffer.empty[HttpHeader]
    tc.tracer.inject(
      tc.span.context(),
      Format.Builtin.TEXT_MAP,
      new AkkaHttpHeaderInjector(traceHeaders)
    ) // inject trace info into headers
    traceHeaders.toList
  }

}

object AkkaHttpTracingSupport extends AkkaHttpTracingSupport
