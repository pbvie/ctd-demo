/*
 * Copyright 2018 Petra Bierleutgeb
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.ontherocks.ctd.tracing.extra

import io.opentracing.Tracer.SpanBuilder
import io.opentracing.log.Fields
import io.opentracing.tag.Tags
import io.opentracing.{ References, Span, SpanContext, Tracer }

import scala.collection.JavaConverters._
import scala.concurrent.{ ExecutionContext, Future }

/**
  * ExTra - ExplicitTracing
  *
  * PoC version, don't use for anything near production :)
  */
trait ExTra {

  import io.ontherocks.ctd.tracing.extra.ExTra.Tag
  import io.ontherocks.ctd.tracing.extra.ExTra.TracingContext

  /**
    * Extend SpanBuilder with the functionality to add a `List` of `Tag`s (key/value pairs).
    */
  implicit class WithTags(spanBuilder: SpanBuilder) {
    def withTags(tags: List[Tag]): SpanBuilder =
      tags.foldLeft(spanBuilder) { (spanBuilder, tag) =>
        spanBuilder.withTag(tag.key, tag.value)
      }
  }

  /**
    * Create and immediately start a new root span.
    */
  def buildRootSpan(operationName: String,
                    tags: List[Tag] = List.empty)(implicit tracer: Tracer): Span =
    tracer.buildSpan(operationName).ignoreActiveSpan().withTags(tags).start()

  def buildChildOf(operationName: String,
                   tracer: Tracer,
                   parentSpanContext: SpanContext,
                   tags: List[Tag] = List.empty): Span =
    tracer.buildSpan(operationName).asChildOf(parentSpanContext).withTags(tags).start()

  /**
    * Create and immediately start a new span with a FOLLOWS_FROM reference.
    */
  def buildFollowsFrom(operationName: String,
                       tracer: Tracer,
                       parentSpanContext: SpanContext,
                       tags: List[Tag] = List.empty): Span =
    tracer
      .buildSpan(operationName)
      .addReference(References.FOLLOWS_FROM, parentSpanContext)
      .withTags(tags)
      .start()

  /**
    * Wrap the given block in a try/finally, providing the given span and makes sure the span is
    * finished at the end of the block - no matter what.
    */
  def withSpan[T](tc: TracingContext)(op: TracingContext => T): T =
    try {
      op(tc)
    } catch {
      case t: Throwable =>
        tc.span.setTag(Tags.ERROR.getKey, true)
        tc.span.log(errorLog(t).asJava)
        throw t
    } finally {
      tc.span.finish()
    }

  /**
    * Wrap the given block in a try/finally, providing the given span and makes sure the span is
    * finished at the end of the block - no matter what.
    */
  def withSpan[T](op: TracingContext => T)(implicit tc: TracingContext): T =
    try {
      op(tc)
    } catch {
      case t: Throwable =>
        tc.span.setTag(Tags.ERROR.getKey, true)
        tc.span.log(errorLog(t).asJava)
        throw t
    } finally {
      tc.span.finish()
    }

  /**
    * Wrap the given block in a try/finally, providing the given span and finishing the span in
    * case of an error.
    */
  def withSpanAndContinue[T](op: Span => T)(implicit tc: TracingContext): T =
    try {
      op(tc.span)
    } catch {
      case t: Throwable =>
        tc.span.setTag(Tags.ERROR.getKey, true)
        tc.span.log(errorLog(t).asJava)
        tc.span.finish()
        throw t
    }

  /**
    * Wrap an async operation and finish the span on completion.
    */
  def withSpanAsync[T](op: TracingContext => Future[T])(implicit ec: ExecutionContext,
                                                        tc: TracingContext): Future[T] =
    op(tc)
      .recoverWith {
        case t: Throwable =>
          tc.span.setTag(Tags.ERROR.getKey, true)
          tc.span.log(errorLog(t).asJava)
          Future.failed(t)
      }
      .andThen {
        case _ =>
          tc.span.finish()
      }

  /**
    * Creates a new child span and provides that span to the given operation. The child span is
    * finished on completion.
    */
  def withChildSpan[T](
      operationName: String,
      tags: List[Tag] = List.empty
  )(op: TracingContext => T)(implicit tc: TracingContext): T = {
    val childSpan           = buildChildOf(operationName, tc.tracer, tc.span.context(), tags)
    val childTracingContext = tc.copy(span = childSpan)
    withSpan(op)(childTracingContext)
  }

  def withChildSpanAsync[T](
      operationName: String,
      tags: List[Tag] = List.empty
  )(op: TracingContext => Future[T])(implicit ec: ExecutionContext,
                                     tc: TracingContext): Future[T] = {
    val childSpan           = buildChildOf(operationName, tc.tracer, tc.span.context(), tags)
    val childTracingContext = tc.copy(span = childSpan)
    withSpanAsync(op)(ec, childTracingContext)
  }

  def withFollowsFrom[T](
      operationName: String,
      tags: List[Tag] = List.empty
  )(op: TracingContext => T)(implicit tc: TracingContext): T = {
    val followingSpan    = buildFollowsFrom(operationName, tc.tracer, tc.span.context(), tags)
    val followingContext = tc.copy(span = followingSpan)
    withSpan(op)(followingContext)
  }

  def withFollowsFromAsync[T](
      operationName: String,
      tags: List[Tag] = List.empty
  )(op: TracingContext => Future[T])(implicit ec: ExecutionContext,
                                     tc: TracingContext): Future[T] = {
    val followingSpan    = buildFollowsFrom(operationName, tc.tracer, tc.span.context(), tags)
    val followingContext = tc.copy(span = followingSpan)
    withSpanAsync(op)(ec, followingContext)
  }

  // internal helper functions
  private def errorLog(t: Throwable): Map[String, Any] = Map(
    Fields.EVENT        -> "error",
    Fields.ERROR_OBJECT -> t
  )

}

object ExTra extends ExTra {

  final case class Tag(key: String, value: String)

  final case class TracingContext(tracer: Tracer, span: Span)

  object TracingContext {

    def withNewRootSpan(operationName: String, tags: List[Tag] = List.empty)(
        implicit tracer: Tracer
    ): TracingContext = {
      val span = buildRootSpan(operationName, tags)
      TracingContext(tracer, span)
    }

    def withFollowsFrom(operationName: String,
                        tracer: Tracer,
                        spanContext: SpanContext,
                        tags: List[Tag] = List.empty): TracingContext = {
      val followingSpan = buildFollowsFrom(operationName, tracer, spanContext, tags)
      TracingContext(tracer, followingSpan)
    }

    def childOf(operationName: String,
                tracer: Tracer,
                spanContext: SpanContext,
                tags: List[Tag] = List.empty): TracingContext = {
      val childSpan =
        buildChildOf(operationName, tracer, spanContext, tags)
      TracingContext(tracer, childSpan)
    }

  }

}
